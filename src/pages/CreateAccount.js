import React from "react";
import { Form, Field } from "react-final-form";
import { TextField, Radio } from "final-form-material-ui";
import {
  Typography,
  Paper,
  Grid,
  Button,
  CssBaseline,
  RadioGroup,
  FormLabel,
  FormControl,
  FormControlLabel
} from "@material-ui/core";

// Date Picker
import DateFnsUtils from "@date-io/date-fns";
import { MuiPickersUtilsProvider, DatePicker } from "material-ui-pickers";

const DatePickerWrapper = (props) => {
  const {
    input: { name, onChange, value, ...restInput },
    meta,
    ...rest
  } = props;
  const showError =
    ((meta.submitError && !meta.dirtySinceLastSubmit) || meta.error) &&
    meta.touched;

  return (
    <DatePicker
      {...rest}
      name={name}
      helperText={showError ? meta.error || meta.submitError : undefined}
      error={showError}
      inputProps={restInput}
      onChange={onChange}
      value={value === "" ? null : value}
    />
  );
}

const CreateAccount = () => {
  const onSubmit = async values => {
    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms));
    await sleep(300);
    window.alert(JSON.stringify(values, 0, 2));
  };

  const validate = values => {
    const errors = {};
    if (!values.firstName) {
      errors.firstName = "Required";
    }
    if (!values.lastName) {
      errors.lastName = "Required";
    }
    if (!values.email) {
      errors.email = "Required";
    }
    if (!values.birthday) {
      errors.birthday = "Required";
    }
    if (values.password !== values.passAgain) {
      errors.password = errors.passAgain = "Do not match";
    }
    if (!values.password) {
      errors.password = "Required";
    }
    if (!values.passAgain) {
      errors.passAgain = "Required";
    }
    return errors;
  };

  return (
    <div style={{ padding: 16, margin: "auto", maxWidth: 600 }}>
      <CssBaseline />
      <Typography variant="h4" align="center" component="h1" gutterBottom>
        Create Account
      </Typography>
      <Form
        onSubmit={onSubmit}
        initialValues={{ privacy: "public" }}
        validate={validate}
        render={({ handleSubmit, reset, submitting, pristine, values }) => (
          <form onSubmit={handleSubmit} noValidate>
            <Paper style={{ padding: 16 }}>
              <Grid container alignItems="flex-start" spacing={8}>
                <Grid item xs={6}>
                  <Field
                    fullWidth
                    required
                    name="firstName"
                    component={TextField}
                    type="text"
                    label="First Name"
                  />
                </Grid>
                <Grid item xs={6}>
                  <Field
                    fullWidth
                    required
                    name="lastName"
                    component={TextField}
                    type="text"
                    label="Last Name"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    name="email"
                    fullWidth
                    required
                    component={TextField}
                    type="email"
                    label="Email"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    name="password"
                    fullWidth
                    required
                    component={TextField}
                    type="password"
                    label="Password"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Field
                    name="passAgain"
                    fullWidth
                    required
                    component={TextField}
                    type="password"
                    label="Re-type Password"
                  />
                </Grid>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <Grid item xs={6}>
                    <Field
                      name="birthday"
                      component={DatePickerWrapper}
                      fullWidth
                      required
                      margin="normal"
                      label="Birthday"
                    />
                  </Grid>
                </MuiPickersUtilsProvider>
                <Grid item style={{ marginTop: 14 }}>
                  <FormControl component="fieldset">
                    <FormLabel component="legend">Privacy</FormLabel>
                    <RadioGroup row>
                      <FormControlLabel
                        label="Public"
                        control={
                          <Field
                            name="privacy"
                            component={Radio}
                            type="radio"
                            value="public"
                          />
                        }
                      />
                      <FormControlLabel
                        label="Private"
                        control={
                          <Field
                            name="privacy"
                            component={Radio}
                            type="radio"
                            value="private"
                          />
                        }
                      />
                    </RadioGroup>
                  </FormControl>
                </Grid>
                <Grid item style={{ marginTop: 16 }}>
                  <Button
                    type="button"
                    variant="contained"
                    onClick={reset}
                    disabled={submitting || pristine}
                  >
                    Reset
                  </Button>
                </Grid>
                <Grid item style={{ marginTop: 16 }}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    disabled={submitting}
                  >
                    Submit
                  </Button>
                </Grid>
              </Grid>
            </Paper>
            <pre>{JSON.stringify(values, 0, 2)}</pre>
          </form>
        )}
      />
    </div>
  );
};

export default CreateAccount;
