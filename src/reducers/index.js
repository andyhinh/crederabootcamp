import { selectedSubreddit, postsBySubreddit } from "./reddit";

export default {
  selectedSubreddit: selectedSubreddit,
  postsBySubreddit: postsBySubreddit
}