import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunkMiddlware from 'redux-thunk';
import { createLogger } from 'redux-logger';

import reducers from './reducers/';

import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

// Will log information about dispatched actions to the console
// including the previous state, the action details, and the next state
const loggerMiddleware = createLogger();

// Setting up the application for the redux development tool on the browser
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  combineReducers({
    ...reducers
	}),
  // compose simply enables us to apply several store enhancers
  // Right now, we are only using applyMiddlware, so this is
  // just future-proofing our application
  composeEnhancers(
    // Middlware can intercept dispatched actions before they reach the reducer
    // in order to modify it in some way
    applyMiddleware(
      // Thunk allows functions to be returned from action creators
      // so we can do things like dispatch multiple actions in a 
      // single action creator for async actions
      thunkMiddlware,
      // logger will output the previous state, next state, and
      // the action details to the console
      loggerMiddleware
    )
  )
);

ReactDOM.render(<App store={store}/>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
