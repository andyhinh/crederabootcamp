import axios from "axios";

export const REQUEST_POSTS = "REQUEST_POSTS";
export const RECEIVE_POSTS = "RECEIVE_POSTS";
export const SELECT_SUBREDDIT = "SELECT_SUBREDDIT";
export const INVALIDATE_SUBREDDIT = "INVALIDATE_SUBREDDIT";
export const FAILED_SUBREDDIT = "FAILED_SUBREDDIT";

export function selectSubreddit(subreddit) {
  return {
    type: SELECT_SUBREDDIT,
    subreddit
  };
}

export function invalidateSubreddit(subreddit) {
  return {
    type: INVALIDATE_SUBREDDIT,
    subreddit
  };
}

function requestPosts(subreddit) {
  return {
    type: REQUEST_POSTS,
    subreddit
  };
}

function receivePosts(subreddit, json) {
  return {
    type: RECEIVE_POSTS,
    subreddit,
    posts: json.data.children.map(child => child.data),
    receivedAt: Date.now()
  };
}

function failSubreddit(subreddit) {
  return {
    type: FAILED_SUBREDDIT,
    subreddit
  };
}

function fetchPosts(subreddit) {
  return async dispatch => {
    dispatch(requestPosts(subreddit));
    return await axios
      .get(`https://www.reddit.com/r/${subreddit}.json`)
      .then(response => {
        // You know why this is important :)
        response.data.data.children = response.data.data.children.filter(
          ele => !ele.data.over_18
        );

        return response.data;
      })
      .then(json => dispatch(receivePosts(subreddit, json)))
      .catch(error => {
        dispatch(failSubreddit(subreddit));
      });
  };
}

function shouldFetchPosts(state, subreddit) {
  const posts = state.postsBySubreddit[subreddit];
  if (!posts) {
    return true;
  } else if (posts.isFetching) {
    return false;
  } else {
    return posts.didInvalidate;
  }
}

export function fetchPostsIfNeeded(subreddit) {
  return (dispatch, getState) => {
    if (shouldFetchPosts(getState(), subreddit)) {
      return dispatch(fetchPosts(subreddit));
    }
  };
}
