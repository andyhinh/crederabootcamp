import React from "react";
import PropTypes from "prop-types";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Navigation from "./components/Navigation";

import Home from "./pages/Home";
import CreateAccount from "./pages/CreateAccount";
import "./App.css";

const App = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Route path="/" component={Navigation} />
      <Route path="/" exact component={Home} />
      <Route path="/create" component={CreateAccount} />
    </Router>
  </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired
};

export default App;
